const provinceApiUrl = 'https://vapi.vnappmob.com/api/province/';
const districtApiUrl = 'https://vapi.vnappmob.com/api/province/district/';
const wardApiUrl = 'https://vapi.vnappmob.com/api/province/ward/';
const optionInfo = {
  selector: '',
  valueKey: '',
  textKey: '',
};
const appendOption = function (list, optionInfo) {
  for (const e of list) {
    $(optionInfo.selector).append(
      `<option value="${e[optionInfo.valueKey]}">${e[optionInfo.textKey]}</option>`
    );
  }
};
const resetSelect = function (selector) {
    $(selector).children('option:first-child').removeAttr('disabled');
    $(selector).children('option:first-child').attr('selected','');
    $(selector).children('option:first-child').nextAll().remove();
};

$('#selected-province').on({
  click: function () {
    $(this).children('option:first-child').attr('disabled', 'disabled');
    $.get(provinceApiUrl, function (data) {
      let provinceList = data.results;
      optionInfo.selector = '#selected-province';
      optionInfo.textKey = 'province_name';
      optionInfo.valueKey = 'province_id';
      appendOption(provinceList, optionInfo);
    });
  },
  change: function () {
    resetSelect('#selected-district');
    resetSelect('#selected-ward');
    const provinceID = $('#selected-province').val();
    $.get(`${districtApiUrl}${provinceID}`, function (data) {
      const districtList = data.results;
      optionInfo.selector = '#selected-district';
      optionInfo.textKey = 'district_name';
      optionInfo.valueKey = 'district_id';
      appendOption(districtList, optionInfo);
    });
  },
});

$('#selected-district').on({
  click: function () {
    $(this).children('option:first-child').attr('disabled', 'disabled');
  },
  change: function () {
    resetSelect('#selected-ward');
    const districtID = $('#selected-district').val();
    $.get(`${wardApiUrl}${districtID}`, function (data) {
      const wardList = data.results;
      optionInfo.selector = '#selected-ward';
      optionInfo.textKey = 'ward_name';
      optionInfo.valueKey = 'ward_id';
      appendOption(wardList, optionInfo);
    });
  },
});

$('#selected-ward').on({
  click: function () {
    $(this).children('option:first-child').attr('disabled', 'disabled');
  },
});
